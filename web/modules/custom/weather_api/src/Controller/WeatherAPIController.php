<?php

/**
 * @file
 */

namespace Drupal\weather_api\Controller;
use function json_decode;

/**
 * Class WeatherAPIController
 * @package Drupal\weather_api\Controller
 */
class WeatherAPIController {

  public $database;

  function __construct(){
    $this->database = \Drupal::database();
  }

  /**
   * Reach out to OpenWeatherMap and return a json with the data.
   * @param string $zip
   * @return string $response
   */
  public function getAPIData(string $zip){

    $response = [];
    $params = sprintf('weather?units=imperial&zip=%s,us&APPID=%s',
      $zip,
      '6cb1a38550bae61366fb858d8cdefdd9'
    );

    try{
      $curl = curl_init();
      curl_setopt_array($curl, array(
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_URL => sprintf('https://api.openweathermap.org/data/2.5/%s', $params),
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_HTTPAUTH => CURLAUTH_BASIC
      ));
      $response['data'] = curl_exec($curl);
      $err = curl_error($curl);
      curl_close($curl);

      if ($err) {
        $response['error'] = 'cURL Error #:' . $err;
      } elseif ($response['data'][0] != '{'){
        $response['error'] = 'No data retrieved.';
      }

      // Check for internal API errors
      $data = json_decode($response['data']);
      if($data->message){
        $response['error'] = $data->message;
      }

    } catch (\Exception $e){
      $response['error'] = $e->getMessage();
    }

    return $response;
  }

  /**
   * Updates the weather_api database.
   * @param array $data
   * @return \Drupal\Core\Database\StatementInterface|int|null
   */
  public function updateDatabase(array $data){
    $fields = array(
    'zip_code' => $data['zip_code'],
    'data' => $data['data'],
    );

    return $this->database->update('weather_api')
    ->fields($fields)
    ->execute();
  }

  /**
   * Get saved weather data.
   * @return array
   */
  public function getSavedData(){
    $query = $this->database
      ->select('weather_api', 'nfd')
      ->fields('nfd', ['zip_code','data']);
    return $query->execute()->fetchAssoc();
  }
}