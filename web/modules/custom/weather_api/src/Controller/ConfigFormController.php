<?php

/**
 * @file
 */

namespace Drupal\weather_api\Controller;


use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigFormController
 * @package Drupal\weather_api\Controller
 */
class ConfigFormController extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'weather_api_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $w = new WeatherAPIController();
    $saved = $w->getSavedData();

    $form['zip_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Location Zip Code'),
      '#description' => $this->t('The provided zip code determines what information is pulled from the OpenWeatherMap API.'),
      '#default_value' => $saved['zip_code']
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   *
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $field_value = $form_state->getValue('zip_code');

    $w = new WeatherAPIController();
    $response = $w->getAPIData($field_value);
    if(!$response['error']){
      \Drupal::logger('weather_api')->notice('Weather API Config form - API data retrieved.');
      $w->updateDatabase([
        'zip_code' => $field_value,
        'data' => $response['data']
      ]);
      \Drupal::logger('weather_api')->notice('Weather API Config form - Database updated.');
    }else{
      \Drupal::logger('weather_api')->alert('Weather API Config form - Data could not be retrieved.');
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * @return array
   */
  public function getEditableConfigNames() {
    return [
      'weather_api.settings'
    ];
  }

}