<?php

namespace Drupal\party_custom_behavior;

use Drupal\Core\Entity\EntityTypeManager;

class PartyNodesService {

  protected $entityTypeManager;

  /**
   * PartyNodesService constructor.
   * @param EntityTypeManager $entity_type_manager
   */
  public function __construct(EntityTypeManager $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Queries Party nodes that are expired, based on the $date parameter.
   * @return \Drupal\Core\Entity\EntityInterface[]
   */
  public function loadExpiredNodes($date) {

    $node_storage = $this->entityTypeManager->getStorage('node');
    $entity_query = $node_storage->getQuery()
      ->condition('type', 'party')
      ->condition('field_ct_p_date', $date, '<');
    $nids = $entity_query->execute();
    return $node_storage->loadMultiple($nids);
  }

}