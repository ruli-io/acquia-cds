<?php

namespace Drupal\node_reference_breadcrumbs\Controller;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\Entity\Node;

class BreadcrumbController implements BreadcrumbBuilderInterface {

  private $links = [];

  /**
   * Gets the module configuration
   * @return \Drupal\Core\Config\ImmutableConfig
   */
  private function getConfig() {
    return \Drupal::config('node_reference_breadcrumbs.settings');
  }

  /**
   * Gets the breadcrumb field machine name as configured under the module settings
   * @return array|mixed|null|string
   */
  private function getBreadcrumbMachineName() {
    $config = $this->getConfig();
    $machineName = $config->get('reference_field') ?? 'field_entity_reference_breadcrumb';
    return $machineName;
  }

  /**
   * Checks if the node passed in has a field for the breadcrumbs
   * @param \Drupal\node\Entity\Node $node
   *
   * @return bool
   */
  private function hasField(Node $node) {
    return $node->hasField($this->getBreadcrumbMachineName());
  }

  /**
   * Creates a link from a Node
   * @param \Drupal\node\Entity\Node $node
   *
   * @return \Drupal\Core\Link
   */
  private function getLink(Node $node) {
    return $node->toLink();
  }

  /**
   * Adds the homepage link to the end of the links. This should be called as the
   * last link in the breadcrumbs.
   */
  private function getHome() {
    $this->links[] = Link::createFromRoute('Home', '<front>');
  }

  /**
   * Recursively traverses through parent nodes and builds out the breadcrumb links
   * @param \Drupal\node\Entity\Node $node
   *
   * @return array
   */
  private function getLinks(Node $node) {
    if(!$node->hasField($this->getBreadcrumbMachineName())) {
      $parent = FALSE;
    } else {
      $parentNid = $node->get($this->getBreadcrumbMachineName())->getValue();
      if($parentNid) {
        $parentNid = $parentNid[0]['target_id'];
        $parent = Node::load((int) $parentNid);
      } else {
        $parent = FALSE;
      }
    }

    $this->links[] = $this->getLink($node);

    if($parent) {
      return $this->getLinks($parent);
    }

    $this->getHome();

    return array_reverse($this->links);
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $attributes) {
    $params = $attributes->getParameters()->all();
    if(!empty($params['node'])) {
      return $this->hasField($params['node']);
    }
    return false;
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $breadcrumb = new Breadcrumb();
    $node = $route_match->getParameter('node');

    $links = $this->getLinks($node);

    foreach($links as $link) {
     $breadcrumb
      ->addLink($link);
    }

    return $breadcrumb;
  }
}
