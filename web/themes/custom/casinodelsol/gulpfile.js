let gulp = require('gulp')
let fs = require('fs')
let path = require('path')

let plugins = {
  autoprefixer: require('gulp-autoprefixer'),
  newer: require('gulp-newer'),
  sass: require('gulp-sass'),
  rename: require('gulp-rename'),
  debug: require('gulp-debug'),
  plumber: require('gulp-plumber'),
  yaml: require('js-yaml'),
  replace: require('gulp-replace'),
  webpack: require('gulp-webpack')
}

let paths = {
  dest: 'templates',
  scripts: [
    'templates/**/**/*.js',
    '!templates/**/**/*.min.js',
    '!templates/_partials/babel-polyfill/*.js',
    '!templates/_partials/systemjs/*.js'
  ],
  styles: 'templates/**/**/*.scss',
  info: 'casinodelsol.info.yml',
  libraries: 'casinodelsol.libraries.yml'
}

let library = function (ret) {
  let info = plugins.yaml.load(fs.readFileSync(paths.info))

  let namespaces = info['component-libraries']
  for (let lib in namespaces) {
    if (!namespaces.hasOwnProperty(lib)) {
      continue
    }
    let stylepath = path.join(__dirname, namespaces[lib].paths[0])
    ret = ret.pipe(plugins.replace(lib, stylepath))
  }
  return ret
}

// gulp.task('scripts', function () {
//   // Run Babel on all scripts
//   return gulp.src(paths.scripts)
//     .pipe(plugins.plumber())
//     .pipe(plugins.webpack(require('./webpack.config.js')))
//     .pipe(gulp.dest('./build'))
// })

gulp.task('styles', function () {
  // Process sass files
  let ret = gulp.src(paths.styles)
    .pipe(plugins.plumber())
    .pipe(plugins.debug())

  ret = library(ret)

  return ret
    .pipe(plugins.sass({
      includePaths: [
        paths.styles,
        'node_modules',
        'node_modules/animatewithsass',
        'node_modules/foundation-sites/scss'
      ],
      outputStyle: 'expanded'
    }))
    .pipe(plugins.autoprefixer({
      browsers: ['last 2 versions', '> 3% in US', 'last 2 Edge versions']
    }))
    .pipe(gulp.dest(paths.dest))
})

// Rerun the task when a file changes
gulp.task('watch', function () {
  gulp.watch(paths.styles, ['styles'])
})

// The default task (called when you run `gulp` from cli)
gulp.task('default', ['watch', 'styles'])
