const path = require('path')

module.exports = {
  cache: false,
  context: __dirname,
  entry: {
    entry: './templates/_partials/global-config/entry.js'
  },
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'scripts.js',
    library: 'CasinoDelSol',
  },
  watch: true,
  externals: {
    // package-name : global-var
    jquery: 'jQuery',
    Drupal: 'Drupal',
    Google: 'window.google',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [
          path.resolve(__dirname, 'templates')
        ],
        loader: 'babel-loader',
        options: {
          presets: ['es2015', 'es2016', 'es2017'],
          plugins: ['transform-es2015-modules-commonjs', 'transform-runtime']
        }
      }
    ]
  },
  resolve: {
    modules: [
      path.resolve(__dirname, 'templates', '_partials'),
      path.resolve(__dirname, 'templates', 'components'),
      path.resolve(__dirname, 'templates', 'blocks'),
      path.resolve(__dirname, 'templates', 'fields'),
      path.resolve(__dirname, 'templates', 'htmls'),
      path.resolve(__dirname, 'templates', 'nodes'),
      path.resolve(__dirname, 'templates', 'pages'),
      path.resolve(__dirname, 'templates', 'paragraphs'),
      path.resolve(__dirname, 'templates', 'regions'),
      path.resolve(__dirname, 'templates', 'views'),
      path.resolve(__dirname, 'node_modules')
    ]
  },
  target: 'web'
}
