import $ from 'jquery'
import {geolocation as Geolocation} from 'Drupal'
import onResizeEnd from 'resize-end'

$(document).ready(function () {
  let getMapMarker = (map) => {
    if (map.mapMarkers && map.mapMarkers.length) {
      return map.mapMarkers[0]
    }
    return false
  }

  onResizeEnd('width', () => {
    let maps = Geolocation.maps
    let Maps = window.google.maps

    // google maps is still loading, don't need to do anything
    if (Geolocation.maps_api_loading) {
      return
    }

    $(maps).each((i, map) => {
      let marker = getMapMarker(map)
      let center = map.googleMap.getCenter()
      let zoom = map.googleMap.getZoom()

      if (marker) {
        center = marker.getPosition()
      }
      Maps.event.trigger(map, 'resize')
      map.googleMap.setZoom(zoom)
      map.googleMap.setCenter(center)
    })
  })
})
