<?php

function casinodelsol_preprocess_block__block_content(&$variables) {
  casinodelsol_add_classes($variables['attributes'], [
    'block--content'
  ]);
}
