<?php

function casinodelsol_preprocess_paragraph__paragraph_accordion_row(&$variables) {
  $variables['attributes'] = casinodelsol_to_attribute($variables['attributes']);

  $variables['attributes']->setAttribute('data-accordion-item', true);
}



function casinodelsol_preprocess_field__field_ca_rows(&$variables) {
}



function casinodelsol_preprocess_field__field_ar_title(&$variables) {
  casinodelsol_to_attribute($variables['attributes']);
  $variables['attributes']->addClass('accordion-title');
  $variables['attributes']->setAttribute('href', '#');

  $variables['tag'] = 'a';
}



function casinodelsol_preprocess_field__field_ar_sections(&$variables) {
  casinodelsol_to_attribute($variables['attributes']);

  $variables['attributes']->setAttribute('data-tab-content', true);
  $variables['attributes']->addClass('accordion-content');
}
