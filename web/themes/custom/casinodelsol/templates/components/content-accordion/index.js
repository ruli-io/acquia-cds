import $ from 'jquery'
import {Accordion} from 'foundation/foundation.js'
import {geolocation as Geolocation} from 'Drupal'

$(document).ready(function () {
  let getDescendantMaps = (accordionPanel) => {
    let maps = []
    $(Geolocation.maps).each((i, container) => {
      if ($.contains(accordionPanel.get(0), container.container[0])) {
        maps.push(container.googleMap)
      }
    })
    return maps
  }

  let reinitMap = (map) => {
    // google maps is still loading, don't need to do anything
    if (Geolocation.maps_api_loading) {
      return
    }

    let Maps = window.google.maps

    // re-initialize the map
    let center = map.getCenter()
    let zoom = map.getZoom()
    Maps.event.trigger(map, 'resize')
    map.setZoom(zoom)
    map.setCenter(center)
  }

  $('[data-accordion]').each(function () {
    (new Accordion($(this)))

    $(this).on('down.zf.accordion', (e, $el) => {
      let maps = getDescendantMaps($el)
      maps.forEach((map) => {
        reinitMap(map)
      })
    })
  })
})