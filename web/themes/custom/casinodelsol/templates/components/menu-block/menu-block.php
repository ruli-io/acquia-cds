<?php

function casinodelsol_preprocess_block__system_menu_block(&$variables) {
  casinodelsol_add_classes($variables['attributes'], [
    'block--menu'
  ]);
}
