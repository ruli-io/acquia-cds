/**
 * @library casinodelsol/zz_partial.typekit
 *
 * Initializes a kit defined in typekit account.
 *
 * @note Typekit object is loaded from external source. Check the library definition. The Typekit global obj as well as this
 *       file are loaded in the head of the document.
 */

try {
  Typekit.load({async: true})
} catch (e) {}