<?php


use Drupal\Component\Utility\Html;

function casinodelsol_preprocess_html(&$variables) {
  $node_type = $variables['node_type'] ?? false;

  casinodelsol_add_classes($variables['html_attributes'], [
    'html',
    ($node_type ? 'html--node--' . Html::getClass($variables['node_type']) : '')
  ]);

  casinodelsol_to_attribute($variables['content_attributes']);
}

function casinodelsol_preprocess_page(&$variables) {
  casinodelsol_add_classes($variables['attributes'], [
    'page',
    ($variables['is_front'] ? 'page--is-front' : 'page--not-front'),
    ($variables['is_admin'] ? 'page--is-accessable-admin' : 'page--not-accesable-admin')
  ]);
}


function casinodelsol_preprocess_region(&$variables) {
  casinodelsol_add_classes($variables['attributes'], [
    'region',
    'region--' . Html::getClass($variables['region'])
  ]);
}

function casinodelsol_preprocess_views_view(&$variables) {
  //  casinodelsol_add_classes($variables['attributes'], [
  //    'js-view-dom-id-' . $variables['dom_id']
  //  ]);
}


function casinodelsol_preprocess_block(&$variables) {
//  kint($variables);
  casinodelsol_add_classes($variables['attributes'], [
    'block',
    'block--' . Html::getClass($variables['base_plugin_id'])
  ]);
}

function casinodelsol_preprocess_node(&$variables) {
  $node = $variables['node'];

  casinodelsol_add_classes($variables['attributes'], array(
    'node',
    'node--type--' . Html::getClass($node->bundle()),
    ($node->isPromoted()
      ? 'node--is-promoted'
      : 'node--not-promoted'),
    ($node->isPublished()
      ? 'node--is-published'
      : 'node--not-published'),
    ($variables['view_mode']
      ? 'node--view-mode node--view-mode--' . Html::getClass($variables['view_mode'])
      : 'node--view-mode node--view-mode--default')
  ));
}


function casinodelsol_preprocess_paragraph(&$variables) {

  casinodelsol_add_classes($variables['attributes'], [
    'paragraph',
    'paragraph--type--' . Html::getClass($variables['paragraph']->bundle()),
    ($variables['view_mode'] ? 'paragraph--view-mode--' . Html::getClass($variables['view_mode']) : '')
  ]);
}


function casinodelsol_preprocess_field(&$variables) {
  casinodelsol_add_classes($variables['attributes'], [
    'field',
    'field--' . Html::getClass($variables['field_name']),
    'field--node--' . Html::getClass($variables['entity_type']),
    ($variables['multiple'] ? 'field--is-multiple' : 'not-multiple field--not-multiple')
  ]);
}