import $ from 'jquery'

const $window = $(window)
let resizeTimer = setTimeout(function () {}, 1) // utility timer
let windowWidth = $window.width()
let windowHeight = $window.height()
$(window).add($('html, body')).on('resize', function() {
  let $window = $(window)
  clearTimeout(resizeTimer)
  resizeTimer = setTimeout(function () {
    if ($window.width() !== windowWidth || $window.height() !== windowHeight) {
      $window.trigger('resizeEnd.zion')
    }
    if ($window.height() !== windowHeight) {
      windowHeight = $window.height()
      $window.trigger('height.resizeEnd.zion')
    }
    if ($window.width() !== windowWidth) {
      windowWidth = $window.width()
      $window.trigger('width.resizeEnd.zion')
    }
  }, 200)
})

export default function onResizeEnd (dimension = null, callback = function () {}) {
  // jump lexical binding for possible arrow function callback
  var cb = function (callable) {
    return callback
  }
  ;(dimension === 'width')
    ? $window.on('width.resizeEnd.zion', cb.call($window, callback))
    : (dimension === 'height')
      ? $window.on('height.resizeEnd.zion', cb.call($window, callback))
      : $window.on('resizeEnd.zion', cb.call($window, callback))
}
