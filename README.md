# Acquia Showcase - Casino Del Sol
---
## Overview

- The client needed a solution that would allow for many different types of content and relationships between content. Their previous solution was built with Wordpress but had issues with security and performance.
- The clear choice was Drupal as it would offer them a fast, secure, and scalable framework to evolve their digital experience.
- We chose to follow an atomic design strategy to showcase the many components of the site.
- **Important:** This isn't the final codebase for the CDS solution. I did not have access to the repository anymore, but luckily had a version of the codebase as it existed mid-project. It is missing the majority of the .twig files for the theme components.

## Key Features

- /web/modules/custom - Contains the various custom modules for the site.
  - **file_browser_custom_behavior:** Additional logic for an entity browser providing content editors a better media experience.
  - **node_reference_breadcrumbs:** Configuration form and logic to allow custom breadcrumb hierarchy in nodes.
  - **party_custom_behavior:** The client needed a custom workflow for parties. It included having a guest list attached to each node, as well as a form to RSVP. Finally, this feature also required fields that would change the way the party was showcased to the end user, so we implemented custom validation for both the admin and the user-facing form.
  - **weather_api:** We needed real-time weather information available in various entities. We chose to use a free weather API and attach it to Drupal's cron to pull forecasts daily. This would limit the amount of times we requested the API and would let us access data with a database query instead of an HTTP request. Then we could use this as an internal service and feed it to anything in Drupal.
